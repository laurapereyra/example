import { TestBed } from '@angular/core/testing';

import { App.AuthguardGuard } from './app.authguard.guard';

describe('App.AuthguardGuard', () => {
  let guard: App.AuthguardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(App.AuthguardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
