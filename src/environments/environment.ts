import {KeycloakConfig} from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
    url: 'http://localhost:8080/auth/',
    realm: 'Demo',
    clientId: 'demo',
    credentials: {
        secret: '41e4a2fb-66f8-4bf8-ab0e-19c9e17a179e'
    }
};

export const environment = {
    production: false,
    keycloakConfig,
    URL: 'http://localhost:5000',
    urlFile: 'http://localhost:5000',
    signUrl: 'https://clibd.cirrus-it.net:7777',
    redirectUri: 'http://localhost:4200/*',
    enableJaroWinkler: false
};
